package com.mycompany.actividacotizacion;

import jodd.http.HttpException;
import org.junit.Test;
import static org.junit.Assert.*;

public class CoinDeskCotizacionRepositoryTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        CoinDeskCotizacionRepository instance = new CoinDeskCotizacionRepository();
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {
        CoinDeskCotizacionRepository instance = new CoinDeskCotizacionRepository();
        instance.setUrl("https://api.coindes.com/v1/bpi/currentprice.json");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

}
