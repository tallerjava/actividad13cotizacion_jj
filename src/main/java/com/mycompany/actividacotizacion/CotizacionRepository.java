package com.mycompany.actividacotizacion;

public abstract class CotizacionRepository {

    public abstract Cotizacion obtenerCotizacion();

    public abstract String getNombre();

}
