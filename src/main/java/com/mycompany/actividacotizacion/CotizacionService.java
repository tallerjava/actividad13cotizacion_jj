package com.mycompany.actividacotizacion;

import java.util.ArrayList;
import java.util.List;

public class CotizacionService {

    private List<CotizacionRepository> repositorios;

    public CotizacionService(List<CotizacionRepository> repositorios) {
        this.repositorios = repositorios;
    }

    public List<Cotizacion> obtenerCotizaciones() {
        List<Cotizacion> cotizaciones = new ArrayList();
        for (CotizacionRepository repositorio : repositorios) {
            try {
                cotizaciones.add(repositorio.obtenerCotizacion());
            } catch (Exception e) {
                cotizaciones.add(new Cotizacion(repositorio.getNombre(), null, null, 0.0f));
            }

        }
        return cotizaciones;
    }

}
